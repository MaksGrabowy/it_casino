from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator

class Profile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    age = models.IntegerField()
    
    def __str__(self):
        return str(self.user)
    
class Balance(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    balance = models.PositiveIntegerField(default=5000)
    ask_for_balance = models.PositiveIntegerField(default=0)

    def __str__(self):
        return str(self.user)
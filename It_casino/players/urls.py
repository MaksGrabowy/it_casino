from django.urls import path
from . import views
from .views import UserEditView
#from django.contrib.auth import views as auth_views

urlpatterns = [
    path("login_user/", views.login_user, name="login"),
    path("logout_user/", views.logout_user, name="logout"),
    path("register_user/", views.register_user, name="register"),
    path("edit_profile/", UserEditView.as_view(), name="edit_profile"),
    #path('<int:uid>/password/', auth_views.PasswordChangeView.as_view()) z jakiegos powodu musi to byc w glownym urls
    path("balance/", views.ask_for_balance, name="balance"),
]
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from .models import Balance, Profile
from django import forms
from django.core.validators import MaxValueValidator, MinValueValidator 

class RegisterUserForm(UserCreationForm):
    age = forms.IntegerField(
                            validators=[MinValueValidator(18),MaxValueValidator(99)], 
                            widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'number', 'min': '18', 'max': '120', 'placeholder': 'Age'}),
                            help_text='Your age must be over 18.'
                            )
    email = forms.EmailField(
                            max_length=100, widget=forms.EmailInput(attrs={'class':'form-control', 'placeholder': 'Email'}),
                            help_text='Enter a valid email address.'
                            )
                            
    class Meta:
        model = User
        fields = ('username', 'age', 'email', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super(RegisterUserForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['username'].widget.attrs['placeholder'] = 'Nickname'
        self.fields['username'].label = ''
        self.fields['username'].help_text = 'Your nickname must contain letters, digits and @/./+/-/_ only.'

        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'Password'
        self.fields['password1'].label = ''

        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['placeholder'] = 'Confirm Password'
        self.fields['password2'].label = ''
        
        self.fields['age'].label = ''
        self.fields['email'].label = ''


class EditProfileForm(UserChangeForm):
    age = forms.IntegerField(
                            validators=[MinValueValidator(18),MaxValueValidator(120)], 
                            widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'number', 'min': '18', 'max': '99', 'placeholder': 'Age'}),
                            help_text='Your age must be over 18.',
                            required=False
                            )
    email = forms.EmailField(
                            max_length=100, widget=forms.EmailInput(attrs={'class':'form-control', 'placeholder': 'Email'}),
                            required=False
                            )
    first_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    last_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    username = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)

    class Meta:
        model = Profile
        fields = ('username', 'age', 'email', 'first_name', 'last_name', 'password')
        #fields = ('age', 'username', 'email')
        

class BalanceForm(forms.ModelForm):
    ask_for_balance = forms.IntegerField(
                            validators=[MinValueValidator(0),MaxValueValidator(5000)], 
                            widget=forms.NumberInput(attrs={'class': 'form-control', 'type': 'number', 'min': '0', 'max': '5000', 'placeholder': 'Enter the balance value you want to receive', 'step': '500'}),
                            )
    class Meta:
        model = Balance
        fields = ['ask_for_balance']
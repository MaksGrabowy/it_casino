from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm  #those are basic forms
from django.contrib.auth.views import PasswordChangeView
from .forms import RegisterUserForm, EditProfileForm, BalanceForm

from django.views import generic 
from django.urls import reverse_lazy

from .models import Profile, Balance
#from .forms import PlayerForm

def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, f"Logged in as {username}!")
            return redirect("home")
        else:
            messages.error(request, ("Login failed! Please try again."))
            return redirect("login")
        
    else:
        return render(request, "authenticate/login.html", {} )

def logout_user(request):
    logout(request)
    messages.error(request, ("You have been logged out."))
    return redirect("login")

""" def register_user(request):
    if request.method == "POST":
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            messages.success(request, ("Registered successfully!"))
            return redirect("login")
    else:
        form = RegisterUserForm()  
    
    return render(request, "authenticate/register.html", {'form':form}) """

def register_user(request):
    if request.method == "POST":
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            user = form.save()

            age = form.cleaned_data['age']
            profile = Profile(user=user, age=age)
            profile.save()

            balance = Balance(user=user)
            balance.save()

            login(request, user)

            messages.success(request, "Registered successfully!")
            return redirect("login")
    else:
        form = RegisterUserForm()

    return render(request, "authenticate/register.html", {'form': form})


class UserEditView(generic.UpdateView):
    form_class = EditProfileForm
    template_name = 'authenticate/edit_profile.html'
    success_url = reverse_lazy('home')

    def get_object(self):
        return self.request.user
    

def balance(reguest):
    return render(reguest, "authenticate/balance.html")


def ask_for_balance(request):
    if request.method == 'POST':
        form = BalanceForm(request.POST)
        if form.is_valid():
            ask_for_balance_instance = form.save(commit=False)
            ask_for_balance_instance.user = request.user

            try:
                existing_balance = Balance.objects.get(user=request.user)
                existing_balance.ask_for_balance = ask_for_balance_instance.ask_for_balance
                existing_balance.save()

            except Balance.DoesNotExist:
                ask_for_balance_instance.save()

            messages.success(request, 'Balance request submitted successfully!')
            return redirect('home') 
        else:
            messages.error(request, 'Error in the balance request form.')
    else:
        form = BalanceForm()

    return render(request, 'authenticate/balance.html', {'form': form})
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator 

# Create your models here.

class Player(models.Model):
    nickname = models.CharField(max_length=20)
    age = models.PositiveSmallIntegerField(validators=[MinValueValidator(18),MaxValueValidator(99)])
    email = models.EmailField(max_length=254)
    password = models.CharField(max_length=50)
    
    def __str__(self):
        return 'Nickname: ' + self.nickname + ' , Email: ' + self.email + ' , Age: ' + str(self.age)




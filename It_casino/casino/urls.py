from django.urls import path
from . import views

from django.contrib.auth import views as auth_views
from .views import PasswordEditView

urlpatterns = [
    path("", views.landing, name = "landing"),
    path("home/", views.home, name = "home"),
    #path("players/", views.players, name = "players"),
    path("roulette/", views.roulette, name = "roulette"),
    #path("register/", views.register, name = "register"),
    path("slot_machine/", views.slot, name = "slot"),
    path("slot_table/", views.slot_table, name = "slot_table"),
    path("htmx/gen_num/", views.do_something, name='Something'),
    path("htmx/gen_nums/", views.get_nums, name='get_nums'),
    path("htmx/balances/", views.get_balance, name='get_balance'),
    path("htmx/buttons/", views.set_button_state, name='set_button'),
    path("htmx/roulette_init/", views.roulette_init, name='set_roulette'),
    path("htmx/roulette_buttons/", views.roulette_button_action, name='roulette_button_action'),
    path("htmx/roulette_spins/", views.spin_roulette, name='spin_roulette'),
    path("profile/", views.profile, name='profile'),
    #path('<int:uid>/password/', auth_views.PasswordChangeView.as_view(template_name='authenticate/change_password.html'))
    path("<int:uid>/password/", PasswordEditView.as_view(template_name='authenticate/change_password.html')),
    path("help/", views.help, name='help'),
    path("htmx/slot_buttons/", views.get_slot_table, name='get_slot_table'),
    path("htmx/slot_spins/", views.spin_slot, name='spin_slot'),
    path("htmx/slot_load/", views.load_slot, name='load_slot'),
    path("baccarat/", views.baccarat, name = "baccarat"),
    path("htmx/bac_bet_set/", views.set_bac_bet, name='set_bac_bet'),
    path("htmx/get_bac_cards/", views.get_bac_cards, name='get_bac_cards'),
    path("htmx/baccarat_plays/", views.play_baccarat, name='play_baccarat'),
    path("htmx/bac_post_result/", views.bac_post_result, name='bac_post_result')
    path("htmx/update_slot/", views.update_slot_balance, name='update_slot_balance'),
    path("credits/", views.credits, name='credits'),
    path("users_list/", views.users_list, name = "users_list"),
    path("animation_test/", views.animation_test, name="animation_test"),
]


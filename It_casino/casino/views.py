from django.shortcuts import render, redirect, HttpResponse
from .models import Player
from players.models import User, Balance
from .forms import PlayerForm
from django.contrib import messages
import random
from itertools import product 


from django.contrib.auth.forms import PasswordChangeForm  #this is basic form
from .forms import EditPasswordForm
from django.contrib.auth.views import PasswordChangeView
from django.urls import reverse_lazy

# Create your views here.
pressed = False

def landing(request):
    return render(request, "welcome.html")

def home(request):
    return render(request, "home.html")

def second(request):
    return render(request, "second_page.html")

def players(request):
    return render(request, "players.html")

def slot(request):
    return render(request, "slot_machine.html")

def slot_table(request):
    return render(request, "partials/slot_table.html")

def roulette(request):
    return render(request, "roulette.html",
                  {"green_color_hex" : "#12A11E",
                   "red_color_hex" : "#E84925"})

def do_something(request):
    num = random.random()
    num_s = str(num)
    context = {
        "number": num_s
    }
    print("dupa")
    return render(request, "partials/num_form.html", context)

def get_nums(request):
    state = request.session['button_pressed']
    if(state == 'not'):
        request.session['button_pressed'] = 'yes'
        num_s = "pressed"
    elif(state == 'yes'):
        request.session['button_pressed'] = 'not'
        num_s = "depressed"
    context = {
        "number": num_s
    }
    print(request.POST.get("name"))
    print(request.POST.get("bet"))
    return render(request, "partials/num_form.html", context)
    #return render(request, "partials/auto_num.html")
    

def set_button_state(request):
    request.session['button_pressed'] = 'not'
    context = {
        "number": "depressed"
    }
    return render(request, "partials/num_form.html", context)

# each roulette button has each own function... pretty tedious...
def roulette_init(request):
    roulette_buttons={}
    for i in range(37):
        name = str(i)
        roulette_buttons[name] = ["0",0]
    
    for j in range(12):
        name = "row_"+str(j)
        roulette_buttons[name] = ["0",0]

    roulette_buttons["first_col"] = ["0",0]
    roulette_buttons["second_col"] = ["0",0]
    roulette_buttons["third_col"] = ["0",0]
    roulette_buttons["1st12"] = ["0",0]
    roulette_buttons["2nd12"] = ["0",0]
    roulette_buttons["3rd12"] = ["0",0]
    roulette_buttons["1-18"] = ["0",0]
    roulette_buttons["19-36"] = ["0",0]
    roulette_buttons["odd"] = ["0",0]
    roulette_buttons["even"] = ["0",0]
    roulette_buttons["red"] = ["0",0]
    roulette_buttons["black"] = ["0",0]

    print(roulette_buttons)
    request.session['button_pressed'] = 'not'
    request.session["roulette_buttons"] = roulette_buttons
    field_names = [];
    field_bets = [];
    for key in roulette_buttons:
        if(roulette_buttons[key][0]=="1"):
            field_names.append(key)
            field_bets.append(roulette_buttons[key][1])
    print(field_names)
    print(field_bets)
    context = {'LUnique': zip(field_names, field_bets)}
    return render(request, "partials/num_form.html", context)

def set_button_state(request):
    request.session['button_pressed'] = 'not'
    context = {
        "number": "depressed"
    }
    request.session['button_pressed'] = 'not'
    context = {
        "number": "depressed"
    }
    return render(request, "partials/num_form.html", context)

def roulette_button_action(request):
    print(request.POST.get("name"))
    print(request.POST.get("bet"))
    name = request.POST.get("name")
    bet = int(request.POST.get("bet"))
    got_buttons = request.session["roulette_buttons"]
    curr_usr = request.user.id;
    curr_usr_balance = Balance.objects.get(user_id=curr_usr).balance
    old_bets = []
    old_names = []
    for key in got_buttons:
        if(got_buttons[key][0]=="1"):
            old_names.append(key)
            old_bets.append(got_buttons[key][1])
    
    new_bet_value = sum(old_bets)+bet
    print("=================")
    print(old_names)
    print(name)
    print("=================")
    if(name not in old_names):
        if(bet > 0 and curr_usr_balance >= new_bet_value):
            new_state = "1"
        else:
            new_state = "0"
    else:
        if(bet > 0 and curr_usr_balance >= new_bet_value):
            new_state = "1"
        elif(bet >0 and curr_usr_balance < new_bet_value):
            new_state = "1"
            bet = got_buttons[name][1]
        elif(bet <= 0):
            new_state = "0"
        else:
            new_state = "1"
    
    got_buttons[name] = [new_state,bet]
    request.session["roulette_buttons"] = got_buttons
    field_names = [];
    field_bets = [];
    for key in got_buttons:
        if(got_buttons[key][0]=="1"):
            field_names.append(key)
            field_bets.append(got_buttons[key][1])
    print(field_names)
    print(field_bets)
    iterator = list(range(len(field_names)))
    context = {'bets': zip(field_names, field_bets, iterator),
               'bet_sum': sum(field_bets)}
    return render(request, "partials/bet_table.html", context)
    #return render(request, "partials/auto_num.html")


def spin_roulette(request):
    winning = 0
    spinned = random.randint(1,36)
    got_buttons = request.session["roulette_buttons"]
    print(got_buttons)
    print("-----------------")
    field_names = []
    field_bets = []
    for key in got_buttons:
        if(got_buttons[key][0]=="1"):
            field_names.append(key)
            field_bets.append(got_buttons[key][1])

    curr_usr = request.user.id;
    curr_usr_balance = Balance.objects.get(user_id=curr_usr).balance
    print("============")
    print(sum(field_bets))
    print(curr_usr_balance)
    print("============")
    if(sum(field_bets) <= curr_usr_balance):
        all_nums = [*range(1,37)]
        print(all_nums)
        print("-----------------")
        red_set = [1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36]
        black_set = [2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35]

        first_col_set = []
        second_col_set = []
        third_col_set = []
        for j in range(1,36,3):
            third_col_set.append(j)
            second_col_set.append(j+1)
            first_col_set.append(j+2)
        print(first_col_set)
        print(second_col_set)
        print(third_col_set)
        print("-----------------")
        first_12 = all_nums[:12]
        second_12 = all_nums[12:24]
        third_12 = all_nums[24:36]

        print(first_12)
        print(second_12)
        print(third_12)
        print("-----------------")

        first_half = all_nums[:18]
        second_half = all_nums[19:]

        print(first_half)
        print(second_half)
        print("-----------------")

        rows = []
        for k in range(12):
            row = [first_col_set[k],second_col_set[k],third_col_set[k]]
            rows.append(row)
        print(rows)
        print("-----------------")

        num_color = 0 # 0 - red, 1 - black 1:1
        num_half = 0 # 1:1
        num_parity = 0 # 1:1
        num_column = 0 # 2:1
        num_triplet = 0 # 2:1
        num_row = 0 # 11:1
        #exact number 35:1
        #splits and other combinations not added yet

        if(spinned in black_set):
            num_color = 1
        elif(spinned in red_set):
            num_color = 0

        if(spinned in first_half):
            num_half = 1
        else:
            num_half = 2

        num_parity = spinned%2
        
        if(spinned in first_col_set):
            num_column = 1
        elif(spinned in second_col_set):
            num_column = 2
        elif(spinned in third_col_set):
            num_column = 3;
        
        if(spinned in first_12):
            num_triplet = 1
        elif(spinned in second_12):
            num_triplet = 2
        elif(spinned in third_12):
            num_triplet = 3;
        
        for i in range(12):
            if(spinned in rows[i]):
                num_row = i+1
                break
        
        print(num_color)
        print(num_column)
        print(num_half)
        print(num_parity)
        print(num_row)
        print(num_triplet)
        print("-----------------")

        field_bets = []
        field_names = []
        for key in got_buttons:
            if(got_buttons[key][0]=="1"):
                field_names.append(key)
                field_bets.append(got_buttons[key][1])
        print(field_names)
        print(field_bets)
        print("------------------")
        winnings = -sum(field_bets)

        bet_on_red = False
        bet_on_black = False
        bet_on_color = False

        if('red' in field_names):
            bet_on_red = True
            bet_on_color = True
        elif('black' in field_names):
            bet_on_black = True
            bet_on_color = True
        
        if(bet_on_color):
            if(bet_on_red and num_color == 0):
                winnings += 2*got_buttons['red'][1]
            elif(bet_on_black and num_color == 1):
                winnings += 2*got_buttons['black'][1]
        
        bet_on_even = False
        bet_on_odd = False

        if('even' in field_names):
            bet_on_even = True
        elif('odd' in field_names):
            bet_on_odd = True


        if(bet_on_even and num_parity == 0):
            winnings += 2*got_buttons['even'][1]
        elif(bet_on_odd and num_parity == 1):
            winnings += 2*got_buttons['odd'][1]


        if('1-18' in field_names and num_half == 1):
            winnings += 2*got_buttons['1-18'][1]
        
        if('19-36' in field_names and num_half == 2):
            winnings += 2*got_buttons['19-36'][1]

        
        if('first_col' in field_names and num_column == 1):
            winnings += 3*got_buttons['first_col'][1]

        if('second_col' in field_names and num_column == 2):
            winnings += 3*got_buttons['second_col'][1]

        if('third_col' in field_names and num_column == 3):
            winnings += 3*got_buttons['third_col'][1]



        if('1st12' in field_names and num_triplet == 1):
            winnings += 3*got_buttons['1st12'][1]

        if('2nd12' in field_names and num_triplet == 2):
            winnings += 3*got_buttons['2nd12'][1]

        if('3rd12' in field_names and num_triplet == 3):
            winnings += 3*got_buttons['3rd12'][1]

        for i in range(12):
            name = "row_"+str(i)
            if(name in field_names and i == num_row):
                winnings += 12*got_buttons[name][1]

        for i in range(36):
            name = str(i)
            if(name in field_names and i == spinned):
                winnings += 36*got_buttons[name][1]

        print("=============")
        print(winnings)
        print("==============")

        curr_usr = request.user.id;
        current_users = list(User.objects.values())
        current_balances = list(Balance.objects.values())
        curr_usr_balance = Balance.objects.get(user_id=curr_usr).balance
        new_balance = curr_usr_balance+winnings
        for e in current_users:
            print(e)
        print("==============")
        print(curr_usr_balance)
        print("==============")

        print(new_balance)
        Balance.objects.filter(user_id=curr_usr).update(balance=new_balance)
        context = {
            "spinned_number" : str(spinned),
            "winning": winnings,
            "actual" : new_balance
        }
        return render(request, "partials/num_form.html", context)
    else:
        context = {
            "spinned_number" : "You don't have enough money to spin. Decrease your bet or request more money",
            "winning" : 0
        }
        return render(request, "partials/num_form.html", context)


def profile(request):
    return render(request, "profile.html")

class PasswordEditView(PasswordChangeView):
    form_class = EditPasswordForm
    success_url = reverse_lazy('home')

def help(request):
    return render(request, "help.html")

def get_slot_table(request):
    request.session['winnings'] = 0
    request.session['result'] = 0
    return render(request,"partials/slot_table.html")

def spin_slot(request):
    number_7 = "7️⃣"      # 0 1:1
    banana =  "🍌"       # 1 1:1
    watermelon = "🍉"    # 2 1:1
    lemon = "🍋"         # 3 1:1
    money = "💰"         # 4 4:1
    bell = "🔔"          # 5 1:1
    orange = "🍊"        # 6 1:1
    aubergine = "🍆"     # 7 3:1
    cherries = "🍒"      # 8 2:1

    reel = [number_7,banana,watermelon,lemon,money,bell,orange,aubergine,cherries]
    reel_nums = []
    bet = int(request.POST.get("bet"))
    curr_usr = request.user.id;
    curr_usr_balance = Balance.objects.get(user_id=curr_usr).balance
    if(bet<= curr_usr_balance):
        winnings = -bet
        for i in range(3):
            reel_nums.append(random.randint(0,8))

        multiplyer = 0
        won = 0
        play_result = "x-1"
        for i in range(9):
            number_of_instances = reel_nums.count(i)
            if(number_of_instances>1):
                multiplyer = number_of_instances-1
                if(i == 4):
                    won = bet + multiplyer*bet*4
                    play_result = "x"+str(multiplyer*4)
                elif(i == 7):
                    won = bet + multiplyer*bet*3
                    play_result = "x"+str(multiplyer*3)
                elif(i == 8):
                    won = bet + multiplyer*bet*2
                    play_result = "x"+str(multiplyer*2)
                else:
                    won = bet + multiplyer*bet
                    play_result = "x"+str(multiplyer)
        winnings+=won
        print(winnings)
        print("==========")
        print(multiplyer)
        print("==========")
        reels = [0,0,0]
        for i in range(3):
            spinned = reel_nums[i]
            indxs = []
            if(spinned == 0):
                spinned_n_1 = 8
                spinned_p_1 = 1
            elif(spinned == 8):
                spinned_n_1 = 7
                spinned_p_1 = 0
            else:
                spinned_p_1 = spinned+1
                spinned_n_1 = spinned-1
            indxs = [spinned_n_1,spinned,spinned_p_1]
            reels[i] = indxs
        
        reels_symbol = [0,0,0]
        for i in range(3):
            symbols = [reel[reels[i][0]],reel[reels[i][1]],reel[reels[i][2]]]
            reels_symbol[i] = symbols
        print(reels)
        print("==========")
        print(reel_nums)
        print("==========")
        print(reels_symbol)

        curr_usr = request.user.id;
        curr_usr_balance = Balance.objects.get(user_id=curr_usr).balance
        new_balance = curr_usr_balance+winnings


        request.session['winnings'] = winnings
        request.session['result'] = play_result


        print("==============")
        print(curr_usr_balance)
        print("==============")

        print(new_balance)
        # Balance.objects.filter(user_id=curr_usr).update(balance=new_balance)
        
        context = {
            "reel_1_top" : str(reels_symbol[0][0]),
            "reel_1_middle" : str(reels_symbol[0][1]),
            "reel_1_bottom" : str(reels_symbol[0][2]),
            "reel_2_top" : str(reels_symbol[1][0]),
            "reel_2_middle" : str(reels_symbol[1][1]),
            "reel_2_bottom" : str(reels_symbol[1][2]),
            "reel_3_top" : str(reels_symbol[2][0]),
            "reel_3_middle" : str(reels_symbol[2][1]),
            "reel_3_bottom" : str(reels_symbol[2][2])
        }
        return render(request,"partials/slot_reels.html",context)
    else:
        default_reel = [lemon,money,bell]
        request.session['winnings'] = 0
        request.session['result'] = "You don't have enough money to play. Decrease your bet or request more money"
        context = {
            "reel_1_top" : str(default_reel[0]),
            "reel_1_middle" : str(default_reel[1]),
            "reel_1_bottom" : str(default_reel[2]),
            "reel_2_top" : str(default_reel[0]),
            "reel_2_middle" : str(default_reel[1]),
            "reel_2_bottom" : str(default_reel[2]),
            "reel_3_top" : str(default_reel[0]),
            "reel_3_middle" : str(default_reel[1]),
            "reel_3_bottom" : str(default_reel[2])
        }
        return render(request,"partials/slot_reels.html",context)

def load_slot(request):
    lemon = "🍋"         # 3
    money = "💰"         # 4
    bell = "🔔"          # 5
    default_reel = [lemon,money,bell]
    context = {
        "spinned_number" : "You don't have enough money to spin. Decrease your bet or request more money",
        "winning" : 0,
        "reel_1_top" : str(default_reel[0]),
        "reel_1_middle" : str(default_reel[1]),
        "reel_1_bottom" : str(default_reel[2]),
        "reel_2_top" : str(default_reel[0]),
        "reel_2_middle" : str(default_reel[1]),
        "reel_2_bottom" : str(default_reel[2]),
        "reel_3_top" : str(default_reel[0]),
        "reel_3_middle" : str(default_reel[1]),
        "reel_3_bottom" : str(default_reel[2])
    }
    return render(request,"partials/slot_reels.html",context)

def update_slot_balance(request):
    winnings = request.session['winnings']
    curr_usr = request.user.id
    play_result = request.session['result']
    curr_usr_balance = Balance.objects.get(user_id=curr_usr).balance
    new_balance = curr_usr_balance+winnings
    print("==============")
    print(new_balance)
    Balance.objects.filter(user_id=curr_usr).update(balance=new_balance)
    context = {
        "spinned_number" : play_result,
        "winning": winnings,
        "actual" : new_balance
    }
    return render(request, "partials/num_form.html", context)

def credits(request):
    return render(request, "credits.html")

def users_list(request):
    users = User.objects.all()
    return render(request, 'users_list.html', {'users': users})

def animation_test(request):
    return render(request, "animation_test.html")

def get_balance(request):
    curr_usr = request.user.id
    curr_usr_balance = Balance.objects.get(user_id=curr_usr).balance
    context = {
        "curr_balance" : str(curr_usr_balance)
    }
    return render(request,"partials/current_balance.html",context)


def baccarat(request):
    request.session['bac_choice'] = "none"
    request.session['bac_bet'] = 0
    return render(request, "baccarat.html")

def get_bac_cards(request):
    default = '✖️'
    context = {
        "face_1" : default,
        "suit_1" : default,
        "face_2" : default,
        "suit_2" : default,
        "face_3" : default,
        "suit_3" : default,
        "face_4" : default,
        "suit_4" : default,
        "face_5" : default,
        "suit_5" : default,
        "face_6" : default,
        "suit_6" : default,
        "initial" : 1
    }
       
    return render(request, "partials/baccarat_cards.html",context)

def set_bac_bet(request):
    betted = int(request.POST.get("bet"))
    chosen = request.POST.get("side")
    print(chosen)
    request.session['bac_choice'] = chosen
    request.session['bac_bet'] = betted
    context = {
        "bet_field" : chosen,
        "bet" : str(betted)
    }
    return render(request, "partials/baccarat_form.html",context)

def play_baccarat(request):
    # basically in baccarat values are just 0-1-2-3-4-5-6-7-8-9 with Ace being the 1 and other figures being 0. with that knowledge our baccarat deck will look like this

    colors = ['♥️', '♦️', '♠️', '♣️']
    face_values = [('🅰️',1),
                   ('2️⃣',2),
                   ('3️⃣',3),
                   ('4️⃣',4),
                   ('5️⃣',5),
                   ('6️⃣',6),
                   ('7️⃣',7),
                   ('8️⃣',8),
                   ('9️⃣',9),
                   ('🤹‍♂️',0),
                   ('👸',0),
                   ('🤴',0)]
    deck = []
    for color in colors:
        for value in face_values:
            deck.append([color,value])
    deck_8 = 8*deck
    random.shuffle(deck_8)
    print("===============")
    print(deck_8[:6])
    player_cards = [deck_8[0],deck_8[2]]
    banker_cards = [deck_8[1],deck_8[3]]
    for i in range(4):
        deck_8.pop(0)
    print("===============")
    print(str(player_cards[0][1][1])+" "+str(player_cards[1][1][1]))
    print(str(banker_cards[0][1][1])+" "+str(banker_cards[1][1][1]))
    print("===============")
    player_has = player_cards[0][1][1]+player_cards[1][1][1]
    if(player_has>=10):
        player_has-=10
    banker_has = banker_cards[0][1][1]+banker_cards[1][1][1]
    if(banker_has>=10):
        banker_has-=10
    print(player_has)
    print(banker_has)
    print("===============")
    player_draws = True
    banker_draws = True

    if(banker_has==8 or banker_has==9 or player_has==8 or player_has==9):
        player_draws = False
        banker_draws = False

    if(player_has==6 or player_has==7):
        player_draws = False
    
    if(player_draws):
        player_cards.append(deck_8[0])
        print("player draws "+str(deck_8.pop(0)))

    if(banker_draws and player_draws and banker_has==3 and player_cards[2][1][1]==8):
        banker_draws = False
    elif(banker_draws and player_draws and banker_has==4 and (player_cards[2][1][1] in [1,8,9,0])):
        banker_draws = False
    elif(banker_draws and player_draws and banker_has==5 and (player_cards[2][1][1] in [1,2,3,8,9,0])):
        banker_draws = False
    elif(banker_draws and player_draws and banker_has==6 and (player_cards[2][1][1] in [1,2,3,4,5,8,9,0])):
        banker_draws = False
    elif(banker_has==7):
        banker_draws=False
    
    if(banker_draws):
        banker_cards.append(deck_8[0])
        print("banker draws "+str(deck_8.pop(0)))

    if(player_draws):
        player_has += player_cards[2][1][1]
    if(player_has>=10):
        player_has-=10

    if(banker_draws):
        banker_has += banker_cards[2][1][1]
    if(banker_has>=10):
        banker_has-=10

    print(player_has)
    print(banker_has)
    print("===============")
    player_distance = 9-player_has
    banker_distance = 9-banker_has
    print(player_distance)
    print(banker_distance)
    print("===============")
    who_won = "none"
    if(player_distance<banker_distance):
        print("Player won")
        who_won = "player"
    elif(player_distance>banker_distance):
        print("Banker won")
        who_won = "banker"
    else:
        print("Tie")
        who_won = "tie"
# now we determined the game winner. we proceed to show results
    face_1 = player_cards[0][1][0]
    suit_1 = player_cards[0][0]
    face_2 = player_cards[1][1][0]
    suit_2 = player_cards[1][0]
    if(player_draws):
        face_3 = player_cards[2][1][0]
        suit_3 = player_cards[2][0]
    else:
        face_3 = '❌'
        suit_3 = '❌'

    face_4 = banker_cards[0][1][0]
    suit_4 = banker_cards[0][0]
    face_5 = banker_cards[1][1][0]
    suit_5 = banker_cards[1][0]
    if(banker_draws):
        face_6 = banker_cards[2][1][0]
        suit_6 = banker_cards[2][0]
    else:
        face_6 = '❌'
        suit_6 = '❌'
    
    context = {
        "face_1" : face_1,
        "suit_1" : suit_1,
        "face_2" : face_2,
        "suit_2" : suit_2,
        "face_3" : face_3,
        "suit_3" : suit_3,
        "face_4" : face_4,
        "suit_4" : suit_4,
        "face_5" : face_5,
        "suit_5" : suit_5,
        "face_6" : face_6,
        "suit_6" : suit_6,
        "initial" : 0
    }
    print("================")
    
    side = request.session['bac_choice']
    value_of_bet = request.session['bac_bet']
    request.session['who_won_bac'] = who_won
    request.session['palyer_card_sum'] = player_has
    request.session['banker_card_sum'] = banker_has
    
    # print("Chosen "+side)
    # print("Won "+who_won)
    # print(value_of_bet)
    print("================")
    return render(request, "partials/baccarat_cards.html",context)

def bac_post_result(request):
    curr_usr = request.user.id
    curr_usr_balance = Balance.objects.get(user_id=curr_usr).balance
    side = request.session['bac_choice']
    value_of_bet = request.session['bac_bet']
    who_won = request.session['who_won_bac']
    player_has = request.session['palyer_card_sum']
    banker_has = request.session['banker_card_sum']
    print("++++++++++++++++++++++++")
    print("Chosen "+side)
    print("Won "+who_won)
    print(value_of_bet)
    print("++++++++++++++++++++++++")

    if(who_won=="player"):
        result_text_1 = "Player wins"
    elif(who_won=="banker"):
        result_text_1 = "Banker wins"
    elif(who_won=="tie"):
        result_text_1 = "Tie"
    game_won = False
    if(side==who_won):
        game_won = True
    
    if(game_won):
        if(who_won=="tie"):
            won_lost = 9*value_of_bet
        else:
             won_lost = value_of_bet
        result_text_2 = "You won!"
    else:
        won_lost = -value_of_bet
        result_text_2 = "You lost"
    new_balance = curr_usr_balance + won_lost
    context = {
        "result_text_1" : result_text_1,
        "result_text_2" : result_text_2,
        "won_lost" : won_lost,
        "current" : new_balance,
        "player_value" : player_has,
        "banker_value" : banker_has
    }
    Balance.objects.filter(user_id=curr_usr).update(balance=new_balance)
    return render(request, "partials/baccarat_result.html",context)
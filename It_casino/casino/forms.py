from django import forms
from .models import Player

from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm

class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = ['nickname', 'age', 'email', 'password',]

class EditPasswordForm(PasswordChangeForm):
    old_password = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'class': 'form-control', 'type': 'password'}), label='Old password')
    new_password1 = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'class': 'form-control', 'type': 'password'}), label='New password')
    new_password2 = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'class': 'form-control', 'type': 'password'}), label='Confirm new password')

    class Meta:
        model = User
        fields = ('old_password', ' new_password1', ' new_password2',)
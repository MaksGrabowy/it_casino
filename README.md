# IT_Casino

Project made by Silesian University of Technology students Maksymilian Grabowy and Jakub Kawalec majoring in Automatic Control and Robotics for IT (Internet Technologies) project

Authors:
- Maksymilian Grabowy
- Jakub Kawalec

IT Group 5, Section 3
subsection: 534

## Scope
This project will feature an virtual casino with virtual currency and gambling games. 
Tools used in project:
- Django framework
- Python language
- HTML
- SQL Database (or other)
- Bootstrap based frontend
- HTMX

## Features
- Baccarat
- Roulette
- Slot machine
- Admin account - user statistics, user account managament
- User account - account editing
- Database with account balance and user information

## Milestones
- [x] Created Gitlab project
- [x] Created basic website (Django) - 19.10.2023
- [x] Connected to database - 26.10.2023
- [x] User account creation - 26.10.2023
- [x] User account balance analisys and management - 2.11.2023
- [x] Improving visual quality - 9.11.2023
- [x] Coded Roulette logic - 16.11.2023
- [x] Coded Baccarat logic - 23.11.2023
- [x] Coded Slot machine logic - 30.11.2023
- [ ] Coded cheat detection - 7.11.2023
- [ ] Testing and improvements - 21.12.2023